from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from sql_server.sql_server import *
from user import *

from config import dp, bot

from aiogram.dispatcher import FSMContext

from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton

def key_def():
    button_1 = KeyboardButton('Войти⏫')
    button_2 = KeyboardButton('Зарегестрироваться')
    key_deff = ReplyKeyboardMarkup().add(button_1,button_2)
    return key_deff


@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    text = """Вас приветсвует Бот комманды ImPulse\nВыберите действие:"""
    await message.reply(text,  reply_markup=key_def())


@dp.message_handler(commands=['help'])
async def process_help_command(message: types.Message):
    await message.reply("Напиши мне что-нибудь, и я отпрпавлю этот текст тебе в ответ!")


@dp.message_handler(text='Зарагестрироваться', state="*")
async def process_start_command(message: types.Message):
    await bot.send_message(message.chat.id, 'Зарегестрируйтесь на сайте - https://localhost')

@dp.message_handler(text=['Отмена❌'], state="*")
async def cancel(message: types.Message, state: FSMContext, text_got='Отмена'):
    await state.finish()
    await bot.send_message(message.chat.id, text = text_got,reply_markup=key_def())

@dp.message_handler(text=['Профиль'], state="*")
async def profile(message: types.Message, state: FSMContext):
    info = get_info(message.chat.id) 
    await bot.send_message(message.chat.id, text = 'Ваш логин:'+info[1]+'\nВас зовут:'+info[0],reply_markup=key_def())

if __name__ == '__main__':
    executor.start_polling(dp,skip_updates=True)