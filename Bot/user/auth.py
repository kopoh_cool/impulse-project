from aiogram import types
from config import dp, bot

from sql_server.sql_server import *

from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup


from q import key_def, cancel
import os

class login_user(StatesGroup):
    waiting_for_user_login = State()
    waiting_for_user_pincode = State()
    waiting_for_user_id = State()

#Скрипт----------------------------------------------------------------
@dp.message_handler(text='Войти⏫', state="*")
async def login(message: types.Message):
    await message.reply("Введите ваш логин с сайта ImPulse")
    await login_user.waiting_for_user_login.set()

@dp.message_handler(state=login_user.waiting_for_user_login, content_types=types.ContentTypes.TEXT)
async def wait_for_login(message: types.Message, state: FSMContext):
    res = check_user_login(message.text)
    if res[0]:
        await state.update_data(username=message.text)
        await state.update_data(first_name=res[1])
        await bot.send_message(chat_id=message.chat.id, text='Здравствуйте, '+res[1]+'\nВведите код с сайта', reply_markup=types.ReplyKeyboardRemove())
        await state.update_data(count=2)
        await login_user.waiting_for_user_pincode.set()
    else:
        markup5 = types.ReplyKeyboardMarkup().row(types.KeyboardButton('Отмена❌'))
        await bot.send_message(chat_id=message.chat.id, text='Такого Логина нет нашей базе данных, проверьте правильность ввода.',reply_markup=markup5)


@dp.message_handler(state=login_user.waiting_for_user_pincode, content_types=types.ContentTypes.TEXT)
async def wait_for_code(message: types.Message, state: FSMContext):
    data = await state.get_data()
    username = data['username']
    print(username)
    if os.path.exists(r'C:\Users\david\Desktop\Django-registration-and-login-system-master/secret'):
        a = open(r'C:\Users\david\Desktop\Django-registration-and-login-system-master/secret', mode='r', encoding='utf-8').read().split('\n')
        print(a)
        for i in a:
            blyat_YA_ZAEBALCYA_PISAT_IMENA_PEREMENNYIM = i.split('-----')
            print(blyat_YA_ZAEBALCYA_PISAT_IMENA_PEREMENNYIM)
            if blyat_YA_ZAEBALCYA_PISAT_IMENA_PEREMENNYIM[0] == username:
                pass_code = blyat_YA_ZAEBALCYA_PISAT_IMENA_PEREMENNYIM[1]
        os.remove(r'C:\Users\david\Desktop\Django-registration-and-login-system-master/secret')
    else:
        await bot.send_message(chat_id=message.chat.id, text='Данный код недействителен,зайдите на сайт, обнововите страницу Profile, и получите новый код ')
        return

    if message.text == pass_code:
        markup5 = types.ReplyKeyboardMarkup().row(types.KeyboardButton('Ок✅'))
        await bot.send_message(chat_id=message.chat.id, text='Вы успешно вошли в свой профиль',reply_markup=markup5)#, reply_markup=types.ReplyKeyboardRemove())
        await login_user.waiting_for_user_id.set()
    else:
        await bot.send_message(chat_id=message.chat.id, text='Данный код недействителен,зайдите на сайт, обнововите страницу Profile, и получите новый код ')
        return


@dp.message_handler(state=login_user.waiting_for_user_id, content_types=types.ContentTypes.TEXT)
async def waiting_for_user_id(message: types.Message, state: FSMContext):
    markup5 = types.ReplyKeyboardMarkup().row(types.KeyboardButton('Посмотреть задачи'))
    markup5.row(types.KeyboardButton('Создать задачи'))
    markup5.row(types.KeyboardButton('Профиль'))
    await bot.send_message(chat_id=message.chat.id, text='cock',reply_markup=markup5)
    data = await state.get_data()
    username = data['username']
    first_name = data['first_name']
    print(message.from_user.id, username, first_name)
    reg_user_in_bot(message.from_user.id, username, first_name)
    await state.finish()
